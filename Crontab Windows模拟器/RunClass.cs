﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Crontab_Windows模拟器
{
    class RunClass
    {
        public static DateTime sUpTime = DateTime.Now;

        public static void RunML(string ml)
        {
            //分 时 日 月 星期 命令
            string[] item = ml.Split(' ');
            if (item.Length < 6) return;
            string minute = item[0]=="*"? DateTime.Now.Minute.ToString():item[0];
            string hour = item[1] == "*" ? DateTime.Now.Hour.ToString() : item[1];
            string day = item[2] == "*" ? DateTime.Now.Day.ToString() : item[2];
            string month = item[3] == "*" ? DateTime.Now.Month.ToString() : item[3];
            string week = item[4] == "*" ? DateTime.Now.DayOfWeek.ToString("d").ToString() : item[4];
            string content = "";
            for(int i=5;i<item.Length;i++)
            {
                content = item[i] + " ";
            }
            content = content.Trim();
            int weekNow = Convert.ToInt32(DateTime.Now.DayOfWeek.ToString("d"));
            int minuteNow = Convert.ToInt32(DateTime.Now.Minute);
            int hourNow = DateTime.Now.Hour;
            int dayNow = DateTime.Now.Day;
            int monthNow = DateTime.Now.Month;
            /**************************************/
            if(weekNow.ToString()==week && minute==minuteNow.ToString() && hour==hourNow.ToString() && day==dayNow.ToString() && month==monthNow.ToString() && week==weekNow.ToString())
            {
                Run(content);
            }
        }

        public static void Run(string ml)
        {
            string strInput = ml;
            Process p = new Process();
            //设置要启动的应用程序
            p.StartInfo.FileName = "cmd.exe";
            //是否使用操作系统shell启动
            p.StartInfo.UseShellExecute = false;
            // 接受来自调用程序的输入信息
            p.StartInfo.RedirectStandardInput = true;
            //输出信息
            p.StartInfo.RedirectStandardOutput = true;
            // 输出错误
            p.StartInfo.RedirectStandardError = true;
            //不显示程序窗口
            p.StartInfo.CreateNoWindow = true;
            //启动程序
            p.Start();
            //向cmd窗口发送输入信息
            p.StandardInput.WriteLine(strInput + "&exit");
            p.StandardInput.AutoFlush = true;
            //获取输出信息
            string strOuput = p.StandardOutput.ReadToEnd();
            //等待程序执行完退出进程
            p.WaitForExit();
            p.Close();
            WriteLog(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " 执行命令【" + ml + "】 返回结果【" + strOuput + "】");
        }

        private static bool isInt(string b)
        {
            try
            {
                int y = Convert.ToInt32(b);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private static bool WriteLog(string log)
        {
            FileStream fs = null;
            string filePath = Application.StartupPath+"/log.txt";
            //将待写的入数据从字符串转换为字节数组
            Encoding encoder = Encoding.UTF8;
            byte[] bytes = encoder.GetBytes(log);
            try
            {
                fs = File.OpenWrite(filePath);
                //设定书写的开始位置为文件的末尾  
                fs.Position = fs.Length;
                //将待写入内容追加到文件末尾  
                fs.Write(bytes, 0, bytes.Length);
            }
            catch
            {
                return false;
            }
            return true;
        }
    }
}
