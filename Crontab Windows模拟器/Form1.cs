﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Crontab_Windows模拟器
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public class PubData{
            public static bool OffPub=false;  //系统开关
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (PubData.OffPub == false)
            {
                button1.Text = "关  闭";
                PubData.OffPub = true;
                if (timer1.Enabled == false)
                {
                    timer1.Interval = 60 * 1000;
                    timer1.Enabled = true;
                }
            }
            else
            {
                button1.Text = "开  启";
                PubData.OffPub = false;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            string ml = textBox1.Text.Trim();
            if(ml.Length<10)
            {
                return;
            }
            string[] mlArr = ml.Split('\n');
            for(int i = 0; i < mlArr.Length; i++)
            {
                string mlItem = mlArr[i].Trim();
                if (mlItem.Length < 10) continue;
                RunClass. RunML(mlItem);
            }
        }
    }
}
